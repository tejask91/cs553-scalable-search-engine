import os
import re
from pyspark import SparkContext
from nltk.stem import *

HOME_DIR = os.getcwd()
DATA_DIR = HOME_DIR + '/data/' #This should be '/data/'
OUTPUT_DIR = HOME_DIR + '/output/' #This should be '/output/'
CODE_DIR = HOME_DIR + '/code/'

sc = SparkContext('local', 'search')
stemmer = PorterStemmer()

index = {}

def oneWordQuery(word):
	word = re.sub(ur"\p{P}+", "", word)
	word = re.sub(ur"[<>.,:;/]", "", word)
	word = stemmer.stem(word)

	if word in index:
		return [docs[0] for docs in index[word]]
	else:
		return []

def freeTextQuery(words):
	'''
	The links are ordered as per the following criteria:
	First links in the intersection -> ordered by word with the least number of links,
	Then unique links for word with the least number of links,
	Then next word with higher number of links and so on...
	For each word, links are sorted by TF-IDF weight
	'''
	#stores the intersection of all URLs
	intersection = set()
	#stores the links for each word in the query
	wordsToLinks = {}
	#For output. Will hold final ordered list of URLs
	orderedLinks = []
	#Set to identify duplicates in the above list
	orderedLinksSet = set()
	
	#This for loop generates the intersection and populates word to URL map
	for i in xrange(len(words)):
		words[i] = re.sub(ur"\p{P}+", "", words[i])
		words[i] = re.sub(ur"[<>.,:;/]", "", words[i])
		words[i] = stemmer.stem(words[i])

		if words[i] in index:
			links = [docs[0] for docs in index[words[i]]]

			if len(intersection) == 0:
				intersection = set(links)
			else:
				intersection = intersection & set(links)
			wordsToLinks[words[i]] = links
	
	#Words are taken in order of least number of links.
	#Creates the orderedLinks list with the URLs in the intersection
	#Also identifies unique links for each word
	for word in sorted(wordsToLinks, key = lambda k: len(wordsToLinks[k])):
		links = [docs[0] for docs in index[word]]
		commonLinks = [link for link in links if link in intersection]

		orderedLinks.append([link for link in commonLinks if link not in orderedLinksSet])
		orderedLinksSet |= set(commonLinks)

		wordsToLinks[word] = [docs[0] for docs in index[word] if docs[0] not in intersection]
	
	#print '\nordered common links: ' + str(orderedLinks)

	#Appends URLs for unique words in order of increasing count
	for word in sorted(wordsToLinks, key = lambda k: len(wordsToLinks[k])):
		orderedLinks.append(wordsToLinks[word])
	
	#Flattens the list before returning
	return [link for sublist in orderedLinks for link in sublist]

def search(query):
	'''
	This method returns the list of links ranked as per the query type
	methods above. This should be called from the search UI with a string of 
	words as the query
	'''

	if query == '':
		return []
	terms = query.split(" ")
	if len(terms) == 1:
		return oneWordQuery(terms[0])
	else:
		return freeTextQuery(terms)

def main():
	'''
	Parses the index file and creates an in-memory dictionary
	main must be called on server start-up.
	'''

	indexFile = sc.pickleFile(OUTPUT_DIR + 'part-00000').collect()

	for entry in indexFile:
		index[entry[0]] = entry[1]
	
	'''
	#print index	
	print 'searching terms...\n\n'
	print 'sky: ',
	print search('sky')
	print '\n\ngreenwood: ',
	print search('greenwood')
	print '\n\nsky greenwood: ',
	print search('sky greenwood')
	'''

if __name__ == '__main__':
	main()
