
# coding: utf-8

# In[90]:

import os
import re
import math
from pyspark import SparkContext
from nltk.stem import *

sc = SparkContext('local', 'indexer')

#Directories where the data files are stored
HOME_DIR = os.getcwd()
DATA_DIR = HOME_DIR + '/data/' #This should be '/data/'
OUTPUT_DIR = HOME_DIR + '/output/' #This should be '/output/'
CODE_DIR = HOME_DIR + '/code/'

spark_home = os.getenv('SPARK_HOME', None)
stopWords = sc.textFile(CODE_DIR + 'stopwords.txt').collect()

def parseDocument(content):
    lines = content.split("\n")
    url = lines[2].split(" ")[1]
	
	#\p{P} strips punctuation from unicode strings    
    line = re.sub(ur"\p{P}+", "", lines[-2])
    line = re.sub(ur"[<>.,:;/]", "", line)

    stemmer = PorterStemmer()
    
    tokens = [token.lower() for token in line.split(" ")]

    words = []
    for w in tokens:
        if w not in stopWords:
            words.append(w)
            
    stemmedWords = [stemmer.stem(w) for w in words]
    
    totalWords = len(stemmedWords)
	    
    count = map(lambda word: (word, 1), stemmedWords)
    red = reduce(lambda a,b : a + b, count)
    res = []
    i = 0
    while i < len(red):
        res.append((red[i], red[i+1]))
        i += 2
    
    res = map(lambda (word, count): ((word, url, totalWords), count), res)
    
    return res



dataFiles = sc.wholeTextFiles(DATA_DIR)

totalDocumentCount = float(dataFiles.count())

wordUrlMap = dataFiles.flatMap(lambda (k,v): parseDocument(v))#.cache()

#print wordUrlMap.collect()
                        
individualWC = wordUrlMap.reduceByKey(lambda a,b : a + b).cache()

#print individualWC.collect()

wordTF = individualWC.map(lambda ((word, url, length), totalCount): (word, (url,float(totalCount)/length)))

#print wordTF.collect()

wordIDF = wordTF\
		.map(lambda (word, (url, tf)): (word, 1))\
		.reduceByKey(lambda a,b : a + b)\
		.map(lambda (word, urlCount): (word, math.log(totalDocumentCount/urlCount, 10)))

#print wordIDF.collect()

index = wordIDF.join(wordTF)\
		.map(lambda (word, (idf,(url, tf))): (word, [(url, idf * tf)]))\
		.reduceByKey(lambda a, b : a + b)\
		.map(lambda (word, l): (word, sorted(l, key = lambda (url, weight) : weight, reverse = True)))

#print index.collect()

#index.coalesce(1).saveAsTextFile(OUTPUT_DIR)
index.coalesce(1).saveAsPickleFile(OUTPUT_DIR)

'''

#globalWC = wordUrlMap.map(lambda ((word, url),v): (word, v)) \
#						.reduceByKey(lambda a,b : a + b).cache()

#print globalWC.collect()

#wordUrlMap.unpersist()

localMap = individualWC.map(lambda ((word, url),v): (word, (url, v))).cache()
#print localMap.collect()

index = localMap.join(globalWC)\
				.map(lambda (word, ((url, local_wc), global_wc)): ((word, global_wc), (url, local_wc)))\
				.reduceByKey(lambda a,b : (a,b))

globalWC.unpersist()
localMap.unpersist()

#print index.collect()

'''
